EXEC=snakes
all:
	go build -tags fyne -o bin/fyne/$(EXEC)
	go build -tags gtk -o bin/gtk/$(EXEC)
setup:
	-mkdir bin
	-mkdir bin/fyne
	-mkdir bin/gtk

clean:
	-rm -rf bin



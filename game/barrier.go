package game

import (
	"errors"
	"fmt"
)

var ErrInvalidBarrierLength = errors.New("Invalid Barrier length")

const (
	minBarrierLength     = 6
	maxBarrierLength     = 32
	defaultBarrierLength = 16
	wallOffset           = 5
)

func (b *Board) NewBarrier(orientation Orientation, length int) error {
	if length < minBarrierLength || length > maxBarrierLength {
		return ErrInvalidBarrierLength
	}
	fmt.Printf("New barrier with orientation %d\n", orientation)
	switch orientation {
	case HORIZONTAL:
		rownum := wallOffset
		colstart := (BOARDSIZE-length)/2 - 1
		ltodo := length
		for c := colstart; ltodo > 0; c++ {
			b.SetState(rownum, c, BARRIER)
			ltodo--
		}
		rownum = BOARDSIZE - wallOffset - 1
		ltodo = length
		for c := colstart; ltodo > 0; c++ {
			b.SetState(rownum, c, BARRIER)
			ltodo--
		}

	case VERTICAL:
		colnum := wallOffset
		rowstart := (BOARDSIZE-length)/2 - 1
		ltodo := length
		for r := rowstart; ltodo > 0; r++ {
			b.SetState(r, colnum, BARRIER)
			ltodo--
		}
		colnum = BOARDSIZE - wallOffset - 1
		ltodo = length
		for r := rowstart; ltodo > 0; r++ {
			b.SetState(r, colnum, BARRIER)
			ltodo--
		}
	}
	return nil
}

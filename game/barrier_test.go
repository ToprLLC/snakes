package game

import (
	"fmt"
	"testing"
)

func TestNewBarrier(t *testing.T) {
	b := NewBoard()
	fmt.Println("Test horizontal")
	err := b.NewBarrier(horizontal, 10)
	if err != nil {
		fmt.Printf("%s", err)
	}
	b.Show()

	b.Clear()
	fmt.Println("Test vertical")
	err = b.NewBarrier(vertical, 12)
	if err != nil {
		fmt.Printf("%s", err)
	}

	b.Show()

}

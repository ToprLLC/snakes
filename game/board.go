package game

import (
	"errors"
	"fmt"
)

var (
	ErrInvalidAddress = errors.New("Invalid row/col address")
)

type SquareState int

const (
	AVAILABLE SquareState = iota
	BARRIER
	OCCUPIED
)

type Direction int

const (
	north Direction = iota
	south
	east
	west
)

type Orientation int

const (
	HORIZONTAL Orientation = iota
	VERTICAL
)

type Board struct {
	squares []SquareState
}

func NewBoard() *Board {
	board := new(Board)

	board.squares = make([]SquareState, BOARDSIZE*BOARDSIZE)
	return board
}

func (b *Board) Clear() {
	for idx, _ := range b.squares {
		b.squares[idx] = AVAILABLE
	}
}

func (b *Board) Idx(row, col int) (int, error) {

	if row >= BOARDSIZE || row < 0 {
		return 0, ErrInvalidAddress
	}
	if col >= BOARDSIZE || col < 0 {
		return 0, ErrInvalidAddress
	}

	return row*BOARDSIZE + col, nil
}

func (b *Board) State(row, col int) (SquareState, error) {
	idx, err := b.Idx(row, col)
	if err != nil {
		return AVAILABLE, err
	}
	return b.squares[idx], nil
}

func (b *Board) SetState(row, col int, state SquareState) error {
	idx, err := b.Idx(row, col)
	if err != nil {
		return err
	}
	b.squares[idx] = state
	return nil
}

func (b *Board) Next(row, col int, dir Direction) (r, c int, err error) {
	_, err = b.Idx(row, col)
	if err != nil {
		return 0, 0, err
	}
	r = row
	c = col
	switch dir {
	case north:
		r = row + 1
	case south:
		r = row - 1
	case east:
		c = c + 1
	case west:
		c = c - 1
	}
	_, err = b.Idx(r, c)
	if err != nil {
		return 0, 0, err
	}
	return r, c, nil
}

func (b *Board) Show() {
	fmt.Printf("Board Size %d\n", len(b.squares))
	for row := 0; row < BOARDSIZE; row++ {
		for col := 0; col < BOARDSIZE; col++ {
			idx, _ := b.Idx(row, col)
			if b.squares[idx] != AVAILABLE {
				fmt.Printf("[%d,%d] = %d\n", row, col, b.squares[idx])
			}
		}
	}
}

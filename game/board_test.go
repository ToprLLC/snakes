package game

import (
	"fmt"
	"testing"
)

func TestBoardCreate(t *testing.T) {
	b := NewBoard()
	b.Show()
	s, _ := b.State(5, 5)
	fmt.Printf("Square %d,%d is %d\n", 5, 5, s)
}

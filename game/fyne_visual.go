// +build fyne

package game

import (
	"fmt"
	"image/color"

	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	"golang.org/x/image/colornames"
)

type FyneVisualSnakes struct {
	VisualGameBase
	MainWindow   fyne.Window
	gameBoard    *fyne.Container
	movesLabel   *widget.Label
	lengthLabel  *widget.Label
	statusLabel  *widget.Label
	pauseButton  *widget.Button
	resumeButton *widget.Button
	growButton   *widget.Button
}

var myapp fyne.App

func (vis *FyneVisualSnakes) Notify(paused bool) UserCommand {
	vis.Refresh()
	temp := command
	command = Continue
	return temp
}

func (vis *FyneVisualSnakes) Init() {
	vis.InitializeBoard()
}

func OnKeyPressed(key *fyne.KeyEvent) {
	cmd := fmt.Sprintf("%s", key.Name)
	fmt.Printf("Keystroke %d\n", key.Name)
	switch cmd {
	case "Up":
		command = North
	case "Down":
		command = South
	case "Right":
		command = East
	case "Left":
		command = West
	case "G", "g":
		command = Grow
	case "P", "p":
		command = Pause
	case "Q", "q":
		//command = Quit
		myapp.Quit()
	case "R", "r":
		command = Resume

	}
}

func (fynevs *FyneVisualSnakes) PaintSquare(row, col int) {

	sqstate, err := fynevs.Game.Board.State(row, col)
	if err != nil {
		fmt.Printf("Row: %d Col %d %s\n", row, col, err)
		return
	}
	x, y := fynevs.Pos(row, col)
	var rect *canvas.Rectangle
	switch sqstate {
	case AVAILABLE:
		//rect = canvas.NewRectangle(colornames.Black)
		rect = canvas.NewRectangle(theme.BackgroundColor())
	case BARRIER:
		rect = canvas.NewRectangle(colornames.Red)
	case OCCUPIED:
		rect = canvas.NewRectangle(colornames.Lightgreen)
	}
	if rect != nil {
		size := fyne.Size{Width: sqWidth, Height: sqHeight}
		rect.Resize(size)
		rect.Move(fyne.Position{
			X: x,
			Y: y,
		})
		fynevs.gameBoard.AddObject(rect)
	}

}

func NewVisualSnakes(orient Orientation, barrier bool) {
	fynevs := new(FyneVisualSnakes)
	myapp = app.New()
	fynevs.MainWindow = myapp.NewWindow("Snakes")
	canvas := canvas.NewRectangle(color.Black)
	canvas.Resize(fyne.Size{BOARDWIDTH + sqWidth, BOARDHEIGHT + sqHeight})
	fynevs.gameBoard = fyne.NewContainer()
	fynevs.gameBoard.Resize(fyne.Size{BOARDWIDTH, BOARDHEIGHT})
	fynevs.MainWindow.Canvas().SetOnTypedKey(OnKeyPressed)

	fynevs.pauseButton = widget.NewButton("Pause", pauseButtonClicked)
	fynevs.resumeButton = widget.NewButton("Resume", resumeButtonClicked)
	fynevs.growButton = widget.NewButton("Grow", growButtonClicked)

	fynevs.movesLabel = widget.NewLabel("Score : %3d ")
	fynevs.lengthLabel = widget.NewLabel("Snake length : %3d ")
	fynevs.statusLabel = widget.NewLabel("Status")
	toolbar := fyne.NewContainerWithLayout(layout.NewHBoxLayout(), fynevs.pauseButton,
		fynevs.resumeButton,
		fynevs.growButton,
		fynevs.movesLabel,
		fynevs.lengthLabel,
		fynevs.statusLabel)

	container := fyne.NewContainer()
	container.AddObject(toolbar)
	container.AddObject(fynevs.gameBoard)

	fynevs.MainWindow.SetContent(container)
	fynevs.MainWindow.Resize(fyne.Size{BOARDWIDTH + sqWidth, BOARDHEIGHT + sqHeight})
	fynevs.Game = NewGame(orient, barrier)
	fynevs.Game.Notifier = fynevs
	go fynevs.Game.Play()

	fynevs.MainWindow.ShowAndRun()

}

func (fynevs *FyneVisualSnakes) InitializeBoard() {

	for row := 0; row < BOARDSIZE; row++ {
		for col := 0; col < BOARDSIZE; col++ {
			fynevs.PaintSquare(row, col)
		}
	}
	fynevs.gameBoard.Refresh()
	fynevs.MainWindow.Canvas().Refresh(fynevs.gameBoard)

}

func (fynevs *FyneVisualSnakes) Refresh() {

	if fynevs.Game.Paused {
		fynevs.statusLabel.SetText("Paused")
		return
	} else {
		fynevs.statusLabel.SetText("Moving")
	}

	fynevs.gameBoard.Objects = nil
	for row := 0; row < BOARDSIZE; row++ {
		for col := 0; col < BOARDSIZE; col++ {
			sqstate, _ := fynevs.Game.Board.State(row, col)
			if sqstate == BARRIER {
				fynevs.PaintSquare(row, col)
			}
		}
	}
	for _, lnk := range fynevs.Game.Snake.links {
		fynevs.PaintSquare(lnk.row, lnk.col)
	}
	pos := fynevs.Game.Snake.prevtailpos
	fynevs.PaintSquare(pos.row, pos.col)
	fynevs.movesLabel.SetText(fmt.Sprintf("Score : %d", fynevs.Game.Score))
	fynevs.lengthLabel.SetText(fmt.Sprintf("Length: %d", fynevs.Game.Snake.Length()))

	fynevs.gameBoard.Refresh()
	fynevs.MainWindow.Canvas().Refresh(fynevs.gameBoard)

}

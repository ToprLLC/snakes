package game

import (
	"fmt"
	"sync"
	"time"
)

const (
	BOARDSIZE = 58
)

type PlayerLevel int

const (
	BEGINNER PlayerLevel = iota
	ADVANCED
)

type Notifier interface {
	Init()
	Notify(paused bool) UserCommand
}

type Game struct {
	Score              int
	BarrierLength      int
	BarrierOrientation Orientation
	Board              *Board
	Snake              *Snake
	Notifier           Notifier
	Paused             bool
	Command            chan string
	done               sync.WaitGroup
}

func (g *Game) Play() {
	g.Notifier.Init()
	for {
		cmd := g.Notifier.Notify(g.Paused)
		if cmd == Continue {
			time.Sleep(1 * time.Second)
			if !g.Paused {
				g.Snake.Move()
				g.Score = g.Score + len(g.Snake.links)
			}
		}
		switch cmd {
		case Pause:
			g.Paused = true
		case Resume:
			g.Paused = false
		case Grow:
			g.Snake.Grow(defaultGrowth)
		case North:
			g.Snake.SetDirection(north)
			fmt.Printf("Turning north")
		case South:
			g.Snake.SetDirection(south)
		case West:
			g.Snake.SetDirection(west)
		case East:
			g.Snake.SetDirection(east)

		}
	}
}

func NewGame(orient Orientation, barrier bool) *Game {
	newgame := new(Game)
	newgame.Board = NewBoard()
	switch orient {
	case HORIZONTAL:
		newgame.Snake, _ = newgame.Board.NewSnake(HORIZONTAL, defaultSnakeLength)
		newgame.BarrierOrientation = VERTICAL
	case VERTICAL:
		newgame.Snake, _ = newgame.Board.NewSnake(VERTICAL, defaultSnakeLength)
		newgame.BarrierOrientation = HORIZONTAL
	}
	if barrier {
		newgame.BarrierLength = defaultBarrierLength
		newgame.Board.NewBarrier(newgame.BarrierOrientation, newgame.BarrierLength)
	}
	newgame.Command = make(chan string)
	return newgame
}

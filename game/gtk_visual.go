// +build gtk

package game

import (
	"fmt"
	"log"

	"github.com/gotk3/gotk3/cairo"
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

const (
	KEY_LEFT  uint = 65361
	KEY_UP    uint = 65362
	KEY_RIGHT uint = 65363
	KEY_DOWN  uint = 65364
)

type GtkVisualSnakes struct {
	VisualGameBase
	mainWindow   *gtk.Window
	gameBoard    *gtk.DrawingArea
	movesLabel   *gtk.Label
	lengthLabel  *gtk.Label
	statusLabel  *gtk.Label
	pauseButton  *gtk.Button
	resumeButton *gtk.Button
	growButton   *gtk.Button
}

var gtkVisualSnakes *GtkVisualSnakes

func OnButtonPressed(win *gtk.Window, event *gdk.Event) {
	keyEvent := &gdk.EventKey{event}
	switch keyEvent.KeyVal() {
	case KEY_LEFT:
		command = West
	case KEY_RIGHT:
		command = East
	case KEY_DOWN:
		command = South
	case KEY_UP:
		command = North
	case 'p', 'P':
		command = Pause
	case 'r', 'R':
		command = Resume
	case 'g', 'G':
		command = Grow
	case 'q', 'Q':
		exitApp()
	default:
		fmt.Printf("%d\n", keyEvent.KeyVal())
		//case gdk.KEY_A, gdk.KEY_a:
		//	fmt.Println("Key A received")
	}
}

func exitApp() {
	fmt.Println("Received destroy event")
	gtkVisualSnakes.mainWindow.Destroy()
	gtk.MainQuit()
}

func (gtkvs *GtkVisualSnakes) Refresh() {

	if gtkvs.Game.Paused {
		gtkvs.statusLabel.SetText("Paused")
	} else {
		gtkvs.statusLabel.SetText("Moving")
	}

	gtkvs.movesLabel.SetText(fmt.Sprintf("Score : %d", gtkvs.Game.Score))
	gtkvs.lengthLabel.SetText(fmt.Sprintf("Length: %d", gtkvs.Game.Snake.Length()))

	gtkvs.mainWindow.QueueDraw()

}
func (gtkvs *GtkVisualSnakes) PaintSquare(row, col int, da *gtk.DrawingArea, cr *cairo.Context) {

	sqstate, err := gtkvs.Game.Board.State(row, col)
	if err != nil {
		fmt.Printf("Row: %d Col %d %s\n", row, col, err)
		return
	}
	x, y := gtkvs.Pos(row, col)
	switch sqstate {
	case AVAILABLE:
		cr.SetSourceRGB(1.0, 1.0, 1.0)
	case BARRIER:
		cr.SetSourceRGB(1.0, 0.0, 0.0)
	case OCCUPIED:
		cr.SetSourceRGB(0.0, 1.0, 0.0)
	}
	cr.Rectangle(float64(x), float64(y), float64(sqWidth), float64(sqHeight))
	cr.Fill()

}
func repaintBoardNull(da *gtk.DrawingArea, cr *cairo.Context) {

}

func repaintBoard(da *gtk.DrawingArea, cr *cairo.Context) {

	for row := 0; row < BOARDSIZE; row++ {
		for col := 0; col < BOARDSIZE; col++ {
			gtkVisualSnakes.PaintSquare(row, col, da, cr)
		}
	}
}
func (gtkvs *GtkVisualSnakes) Notify(paused bool) UserCommand {
	temp := command
	command = Continue
	return temp
}

func (gtkvis *GtkVisualSnakes) Init() {

}

func visualUpdates() bool {
	gtkVisualSnakes.Refresh()
	return true
}
func NewVisualSnakes(orient Orientation, barrier bool) {

	gtkVisualSnakes = new(GtkVisualSnakes)
	gtkvs := gtkVisualSnakes
	gtkvs.Game = NewGame(orient, barrier)
	gtkvs.Game.Notifier = gtkvs

	gtk.Init(nil)
	var err error
	gtkvs.mainWindow, err = gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		log.Fatal("Unable to create window:", err)
	}
	gtkvs.mainWindow.SetTitle("Snakes using GTK")
	gtkvs.mainWindow.Connect("destroy", exitApp)

	gtkvs.pauseButton, _ = gtk.ButtonNewWithLabel("Pause")
	gtkvs.pauseButton.Connect("clicked", pauseButtonClicked)

	gtkvs.resumeButton, _ = gtk.ButtonNewWithLabel("Resume")
	gtkvs.resumeButton.Connect("clicked", resumeButtonClicked)

	gtkvs.growButton, _ = gtk.ButtonNewWithLabel("Grow")
	gtkvs.growButton.Connect("clicked", growButtonClicked)

	gtkvs.movesLabel, _ = gtk.LabelNew("Moves %d")
	gtkvs.lengthLabel, _ = gtk.LabelNew("Length %d")
	gtkvs.statusLabel, _ = gtk.LabelNew("Status")

	menuBar, _ := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 5)
	menuBar.Add(gtkvs.pauseButton)
	menuBar.Add(gtkvs.resumeButton)
	menuBar.Add(gtkvs.growButton)
	menuBar.Add(gtkvs.movesLabel)
	menuBar.Add(gtkvs.lengthLabel)
	menuBar.Add(gtkvs.statusLabel)

	gtkvs.gameBoard, _ = gtk.DrawingAreaNew()
	gtkvs.gameBoard.SetSizeRequest(BOARDWIDTH, BOARDHEIGHT)
	gtkvs.gameBoard.Connect("draw", repaintBoard)

	/*win.Connect("key-press-event", func(win *gtk.Window, ev *gdk.Event) {
		keyEvent := &gdk.EventKey{ev}
		if move, found := keyMap[keyEvent.KeyVal()]; found {
			move()
			win.QueueDraw()
		}
	})*/

	gtkvs.gameBoard.AddEvents(int(gdk.BUTTON_PRESS_MASK))

	fullwin, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	fullwin.Add(menuBar)
	fullwin.Add(gtkvs.gameBoard)
	gtkvs.mainWindow.Add(fullwin)
	gtkvs.mainWindow.Connect("key-press-event", OnButtonPressed, nil)
	gtkvs.mainWindow.SetDefaultSize(WINWIDTH, WINHEIGHT)
	gtkvs.mainWindow.ShowAll()

	glib.TimeoutAdd(1000, visualUpdates)
	fmt.Println("Initialized Gtk")

	go gtkvs.Game.Play()
	gtk.Main()
}

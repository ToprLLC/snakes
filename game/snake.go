package game

import (
	"errors"
	"fmt"
)

var ErrInvalidSnakeLength = errors.New("Invalid snake length")
var ErrCannotMove = errors.New("Cannot Move")

const (
	minLength          = 5
	maxLength          = 20
	defaultSnakeLength = 8
	defaultGrowth      = 2
)

type link struct {
	row int
	col int
}

type Snake struct {
	board       *Board
	links       []link
	prevtailpos link
	forward     Direction
}

func (b *Board) NewSnake(orientation Orientation, length int) (*Snake, error) {
	if length < minLength || length > maxLength {
		return nil, ErrInvalidSnakeLength
	}
	fmt.Printf("New snake with orientation %d\n", orientation)
	snake := new(Snake)
	snake.board = b
	snake.links = make([]link, length)
	pos := link{row: BOARDSIZE / 2, col: BOARDSIZE / 2}
	snake.links[0] = pos
	b.SetState(pos.row, pos.col, OCCUPIED)

	//fmt.Printf("Row: %d Col %d\n", snake.links[0].row, snake.links[0].col)
	for seg := 1; seg < length; seg++ {
		switch orientation {
		case HORIZONTAL:
			pos.col = pos.col - 1
		case VERTICAL:
			pos.row = pos.row - 1
		}
		snake.links[seg] = pos
		b.SetState(pos.row, pos.col, OCCUPIED)
		//fmt.Printf("Row: %d Col %d\n", snake.links[seg].row, snake.links[seg].col)
	}
	switch orientation {
	case HORIZONTAL:
		snake.forward = north
	case VERTICAL:
		snake.forward = west
	}

	return snake, nil
}

func (s *Snake) Move() error {
	head := s.links[0]
	nextrow, nextcol, err := s.board.Next(head.row, head.col, s.forward)
	if err != nil {
		return err
	}

	st, _ := s.board.State(nextrow, nextcol)
	if st != AVAILABLE {
		return ErrCannotMove
	}
	movedfrom := s.links[0]
	s.links[0] = link{nextrow, nextcol}
	s.board.SetState(nextrow, nextcol, OCCUPIED)
	for lnk := 1; lnk < len(s.links); lnk++ {
		temp := s.links[lnk]
		if lnk == len(s.links)-1 {
			s.board.SetState(s.links[lnk].row, s.links[lnk].col, AVAILABLE)
			s.prevtailpos = s.links[lnk]
		}
		s.links[lnk] = movedfrom
		movedfrom = temp
	}

	return nil
}

func (s *Snake) SetDirection(newdir Direction) error {
	head := s.links[0]
	nextrow, nextcol, err := s.board.Next(head.row, head.col, newdir)
	if err != nil {
		return err
	}
	st, _ := s.board.State(nextrow, nextcol)
	if st != AVAILABLE {
		return ErrCannotMove
	}

	s.forward = newdir
	return nil
}

func (s *Snake) Grow(by int) error {

	if len(s.links)+by > maxLength {
		return ErrInvalidSnakeLength
	}

	var newdir Direction
	switch s.forward {
	case north:
		newdir = south
	case south:
		newdir = north
	case east:
		newdir = west
	case west:
		newdir = east
	}
	curr := s.links[len(s.links)-1]
	for i := by; i > 0; i-- {
		nextrow, nextcol, err := s.board.Next(curr.row, curr.col, newdir)
		if err != nil {
			return err
		}
		s.links = append(s.links, link{nextrow, nextcol})
		s.board.SetState(nextrow, nextcol, OCCUPIED)
		curr = link{nextrow, nextcol}
	}
	return nil
}

func (s *Snake) Length() int {
	return len(s.links)
}
func (s *Snake) Show() {
	fmt.Printf("Snake Length = %d Moving Direction %d\n", len(s.links), s.forward)
	for idx, seg := range s.links {
		fmt.Printf("%d : [%d,%d]\n", idx, seg.row, seg.col)
	}
}

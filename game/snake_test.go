package game

import (
	"fmt"
	"testing"
)

func TestNewSnake(t *testing.T) {
	b := NewBoard()
	fmt.Println("Test horizontal")
	s, _ := b.NewSnake(horizontal, 10)
	b.Show()
	s.Show()
	b.Clear()

	fmt.Println("Test vertical")
	s, _ = b.NewSnake(vertical, 5)
	b.Show()
	s.Show()
}

func TestMove(t *testing.T) {
	fmt.Println("Test Moves")
	b := NewBoard()
	s, _ := b.NewSnake(horizontal, 10)

	for i := 1; ; i++ {
		err := s.Move()
		if err != nil {
			fmt.Printf("%s\n", err)
			break
		}
		fmt.Printf("Moved %d times\n", i)
	}
	b.Show()
	s.Show()
}

func TestSetDirection(t *testing.T) {
	fmt.Println("Test SetDirection")
	b := NewBoard()
	s, _ := b.NewSnake(vertical, 10)

	for i := 1; i <= 5; i++ {
		err := s.Move()
		if err != nil {
			fmt.Printf("%s\n", err)
			break
		}
		fmt.Printf("Moved %d times\n", i)
	}
	s.Show()

	s.SetDirection(east)
	fmt.Println("Changed direction")
	for i := 1; i <= 5; i++ {
		err := s.Move()
		if err != nil {
			fmt.Printf("%s\n", err)
			break
		}
		fmt.Printf("Moved %d times\n", i)
	}
	s.Show()

	s.SetDirection(south)
	fmt.Println("Changed direction again")
	for i := 1; i <= 5; i++ {
		err := s.Move()
		if err != nil {
			fmt.Printf("%s\n", err)
			break
		}
		fmt.Printf("Moved %d times\n", i)
	}
	s.Show()
	b.Show()
}

func TestGrow(t *testing.T) {
	fmt.Println("Test Grow")
	b := NewBoard()
	s, _ := b.NewSnake(vertical, 10)

	for i := 1; i <= 5; i++ {
		err := s.Move()
		if err != nil {
			fmt.Printf("%s\n", err)
			break
		}
		fmt.Printf("Moved %d times\n", i)
	}
	s.Show()
	s.SetDirection(east)
	fmt.Println("Changed direction again")
	for i := 1; i <= 5; i++ {
		err := s.Move()
		if err != nil {
			fmt.Printf("%s\n", err)
			break
		}
		fmt.Printf("Moved %d times\n", i)
	}
	s.Show()
	err := s.Grow(5)
	if err != nil {
		t.Error(err)
	}
	fmt.Println("Snake grew once")
	s.Show()
}

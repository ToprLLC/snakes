package game

const (
	sqHeight = 10
	sqWidth  = 10
)

var BOARDWIDTH = sqWidth * BOARDSIZE
var BOARDHEIGHT = sqHeight * BOARDSIZE
var WINWIDTH = BOARDWIDTH
var WINHEIGHT = BOARDHEIGHT + 30

type VisualGameBase struct {
	Game   *Game
	Paused bool
}

type UserCommand int

const (
	None UserCommand = iota
	Pause
	Resume
	North
	South
	East
	West
	Quit
	Grow
	Continue
)

func (vis *VisualGameBase) Pos(row, col int) (x, y int) {
	x = col * sqWidth
	y = BOARDHEIGHT - row*sqHeight - sqHeight
	//y = BOARDHEIGHT - row*sqHeight
	return x, y
}

func pauseButtonClicked() {
	command = Pause
}

func resumeButtonClicked() {
	command = Resume
}

func growButtonClicked() {
	command = Grow
}

var maingame *Game
var command UserCommand

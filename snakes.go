package main

import (
	"flag"
	"fmt"

	"bitbucket.org/snakes/game"
)

func main() {
	flag.Parse()
	if flag.NArg() < 1 {
		fmt.Println("usage: snakes [horizontal|vertical] [barriers]")
		fmt.Println("              horizontal|vertical - initial orientation of the snake")
		fmt.Println("              barriers            - include barriers")
		fmt.Println("defaults:     horizontal and no barriers")
		fmt.Println("at least one argument is required - use . if you dont want any overrides")
		return
	}
	//var newgame *game.FyneVisualSnakes
	var orient game.Orientation
	var barriers bool
	orient = game.HORIZONTAL
	for _, arg := range flag.Args() {
		switch arg {
		case "horizontal":
			orient = game.HORIZONTAL
		case "vertical":
			orient = game.VERTICAL
		case "barriers":
			barriers = true
		}
	}
	game.NewVisualSnakes(orient, barriers)

}

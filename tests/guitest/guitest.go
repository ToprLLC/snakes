package main

import (
	"fmt"

	"bitbucket.org/snakes/game"
)

func main() {
	fynevs := game.NewVisualSnakes(game.HORIZONTAL, true)
	fynevs.Game.Board.SetState(0, 0, game.OCCUPIED)
	fynevs.Game.Board.SetState(0, game.BOARDSIZE-1, game.OCCUPIED)
	fynevs.Game.Board.SetState(game.BOARDSIZE-1, 0, game.OCCUPIED)
	fynevs.Game.Board.SetState(game.BOARDSIZE-1, game.BOARDSIZE-1, game.OCCUPIED)

	err := fynevs.Game.Board.NewBarrier(game.HORIZONTAL, 10)
	if err != nil {
		fmt.Printf("%s\n", err)
	}
	err = fynevs.Game.Board.NewBarrier(game.VERTICAL, 15)
	if err != nil {
		fmt.Printf("%s\n", err)
	}

	fynevs.Game.Snake, err = fynevs.Game.Board.NewSnake(game.HORIZONTAL, 22)
	if err != nil {
		fmt.Printf("%s\n", err)
	}

	fynevs.Refresh()
	fynevs.MainWindow.ShowAndRun()
}
